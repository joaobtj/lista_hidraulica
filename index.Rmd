```{r, include=FALSE}
source("link.R")
```


# Hidrostática

São 17 exercícios da temática Hidrostática

```{r, echo=FALSE, results="asis"}

# descrição dos arquivos
desc <- c(
  "Pressão na base do tanque",
  "Dois manômetros em profundidades",
  "Pressão fundo tanque",
  "Densidade do óleo",
  "Pressão coluna fluido",
  "Altura piezômetro",
  "Altura fluido tubo em U",
  "Pressão tubo em U",
  "Peso causa pressão",
  "Altura da água na caixa d'água com torneira fechada",
  "Altitude com barômetro",
  "Altura fluido tubo em U",
  "Pressão no tubo em U",
  "Peso especifico tubo em U",
  "Diferença pressão manômetro diferencial",
  "Diferença pressão manômetro diferencial",
  "Altura manômetro tubo em U"
)

# listar os arquivos 
files_hidrostatica <- list.files(path="hidrostatica/", 
                                 pattern = "Rmd")


for (i in files_hidrostatica){
  link(paste0(i,": ", desc[match(i,files_hidrostatica)]), paste0("hidrostatica/",i), inline=FALSE)
  }
```

No link abaixo você pode fazer o download (em formato zip) de todos os exercícios em formato Rmd, assim como das imagens presentes nestes exercícios.

[Todos os exercícios: Hidrostática](https://gitlab.com/joaobtj/lista_hidraulica/-/archive/main/lista_hidraulica-main.zip?path=hidrostatica)

Para criar o arquivo XML/Moodle com os exercícios, você pode usar estes comandos do pacote `exams` em um script em R.

````r
l3 <- formatC(1:17, width=2, flag="0")
ex <- c()
for (i in l3) {
  ex[i] <- paste0("3-", i, ".Rmd")
}

#xml para moodle
ex3 <- exams::exams2moodle(
  ex,
  edir = "hidrostatica",
  n = 100,
  encoding = "UTF-8",
  verbose = TRUE,
  name = "Hidrostatica"
)
````






# Hidrodinâmica

São 17 exercícios da temática Hidrodinâmica

```{r, echo=FALSE, results="asis"}

# descrição dos arquivos
desc <- c(
"Vazão",
"Vazão e velocidade",
"Vazao na câmara com três saídas",
"Vazao na saída do reservatório com duas entradas",
"Velocidada em dois trechos",
"Velocidade bomba gasolina",
" ",
"Diâmetro",
"Altura do sifão",
"Pressão em um sifão",
"Altura que estabiliza em função das vazões de entrada e saída",
"Altura do jato d'água",
"Vazão em bocal em função da pressão",
"",
"Vazão em bocal sob pressão",
"Vazão dividida",
"Pitot em um barco"
)

# listar os arquivos 
files_hidrodinamica <- list.files(path="hidrodinamica/", 
                                 pattern = "Rmd")


for (i in files_hidrodinamica){
  link(paste0(i,": ", desc[match(i,files_hidrodinamica)]), paste0("hidrodinamica/",i), inline=FALSE)
  }
```

No link abaixo você pode fazer o download (em formato zip) de todos os exercícios em formato Rmd, assim como das imagens presentes nestes exercícios.

[Todos os exercícios: Hidrodinâmica](https://gitlab.com/joaobtj/lista_hidraulica/-/archive/main/lista_hidraulica-main.zip?path=hidrodinamica)

Para criar o arquivo XML/Moodle com os exercícios, você pode usar estes comandos do pacote `exams` em um script em R.

````r
l4 <- formatC(1:17, width=2, flag="0")
ex <- c()
for (i in l4) {
  ex[i] <- paste0("4-", i, ".Rmd")
}

#xml para moodle
ex4 <- exams::exams2moodle(
  ex,
  edir = "hidrodinamica",
  n = 100,
  encoding = "UTF-8",
  verbose = TRUE,
  name = "Hidrodinamica"
)
````