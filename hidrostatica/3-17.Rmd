
```{r dgp, include = FALSE} 
library(exams)
library(magrittr)
library(knitr)
options(scipen = 999) #prevent scientific notation 

#incluir figura
include_supplement("3-17.png")

## RANDOM DATA 
z <- 15:25 %>% sample(1)

## SOLUTION
h <- (z / 100 * 13.6 + 0.15)

```

Question
========
Um manômetro de mercúrio está instalado em um tanque como na figura abaixo. Encontre a altura de água (H, em m) no tanque, quando a altura z for igual a `r z` cm.  

![](3-17.png){width=450px}    

Solution
========
A altura H é igual a `r fmt(h)` m.  

H = `r z/100` * 13.6 + 0.15 = `r fmt(h)`


Meta-information
================
extype: num
exsolution: `r h`
exname: altura
extol: 0.2