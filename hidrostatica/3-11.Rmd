
```{r dgp, include = FALSE} 
library(exams)
library(magrittr)
library(knitr)
options(scipen = 999) #prevent scientific notation 


## RANDOM DATA 
p <- 570:690 %>% sample(1)

## SOLUTION
alt <- (760 * 13.6 - p * 13.6) / 1.125

```

Question
========
Um barômetro de mercúrio marca 760 mm. Ao mesmo tempo, outro, no alto de uma montanha, marca `r p` mm. Supondo o peso específico do ar constante e igual a 1,125 kgf/m^3^, qual é a diferença de altitude? 
  
Solution
========
A diferença de altitude é de `r fmt(alt)` m.  

h = (760 * 13.6 - `r p` * 13.6)/1.125 = `r fmt(alt)`



Meta-information
================
extype: num
exsolution: `r alt`
exname: barometro
extol: 2.0