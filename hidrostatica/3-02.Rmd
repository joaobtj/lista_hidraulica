
```{r dgp, include = FALSE} 
library(exams)
library(magrittr)
library(knitr)
options(scipen = 999) #prevent scientific notation 

## RANDOM DATA
repeat{
  
#h2<h1
h <- 3:12 %>% sample(2) %>% sort(decreasing = TRUE)

#p2>p1
p <- seq(51,91,0.1) %>% sample(2) %>% sort(decreasing = FALSE)

gama <- ((p[2] - p[1]) / (h[1] - h[2])) %>% round(2)
ro <- ((p[2] - p[1]) / (h[1] - h[2]) * 1000 / 9.81) %>% round(2)
d <- ((p[2] - p[1]) / (h[1] - h[2]) * 1000 / 9.81 / 1000) %>% round(3)

if (d > 0.7)
  break
}

## questions/answer
questions <- character(3)
solutions <- logical(3)
explanations <- character(3)

#equations in explanations
eqgama <- paste0("(",p[2]," - ",p[1],")/(",h[1]," - ",h[2],") = ",gama," kN/m^3^")
eqro <- paste0("(",p[2]*1000," - ",p[1]*1000,")/(",h[1]," - ",h[2],")/9.81 = ",ro," kg/m^3^")
eqd <- paste0("(",p[2]," - ",p[1],")/(",h[1]," - ",h[2],")/9.81 = ",d)

while (!any(solutions)){ #impedir todas falsas
  
  um <- c(gama, round(gama*9.81,2), round(gama/9.81,2)) %>% sample(1)
  questions[1] <- paste("O peso específico do líquido é igual a",um," kN/m^3^")
  solutions[1] <- gama == um
  explanations[1] <- paste("O peso específico do líquido é igual a ",eqgama)

  dois <- sample(c(ro,round(ro*9.81,2),round(ro/9.81,2)),1)
  questions[2] <- paste("A massa específica do líquido é igual a",dois," kg/m^3^")
  solutions[2] <- ro == dois
  explanations[2] <- paste("A massa específica do líquido é igual a",eqro)

  tres <- sample(c(d,round(runif(1,0.5,3),2)),1)
  questions[3] <- paste("A densidade do líquido é igual a",tres)
  solutions[3] <- d == tres
  explanations[3] <- paste("A densidade do líquido é igual a",eqd)
}

o <- sample(1:3)
questions <- questions[o]
solutions <- solutions[o]
explanations <- explanations[o]

```

Question
========
Um manômetro instalado a `r h[1]` m acima do fundo de um tanque contendo um certo líquido mede uma pressão de `r p[1]` kPa. Outro manômetro instalado a `r h[2]` m acima do fundo mede `r p[2]` kPa.  
Assinale todas as afirmativas corretas:

```{r questionlist, echo = FALSE, results = "asis"}
answerlist(questions, markup = "markdown")
```

Solution
========

```{r solutionlist, echo = FALSE, results = "asis"}
answerlist(ifelse(solutions, "CORRETA", "FALSA"), explanations, markup = "markdown")
```

Meta-information
================
extype: mchoice
exsolution: `r mchoice2string(solutions)`
exname: manometro