
```{r dgp, include = FALSE} 
library(exams)
library(magrittr)
library(knitr)
options(scipen = 999) #prevent scientific notation 

#incluir figura
include_supplement("4-04.png")

## RANDOM DATA 
d <- 40:60 %>% sample(3)
q <- 10:20 %>% sample(2)
q <- c(q[1],sum(q),q[2])

## SOLUTION
v <- c(
  (q[1]/1000)/(pi*(d[1]/1000)^2/4),
  (q[2]/1000)/(pi*(d[2]/1000)^2/4),
  (q[3]/1000)/(pi*(d[3]/1000)^2/4)) %>% round(2)


```

Question
========
O tanque da figura abaixo é abastecido através da seção 1 (D~1~ = `r d[1]` mm) com água a uma velocidade de `r v[1]` m/s e através da seção 3 com uma vazão de `r q[3]` L/s. Se o nível da água no tanque é mantido constante, qual a velocidade de saída da água na seção 2 (D~2~ = `r d[2]` mm)?. 

*OBS: desprezar as perdas de energia*

![](4-04.png){width=450px} 


Solution
========
A velocidade de saída da água na seção 2 é igual a `r v[2]` m/s  

v2 = (pi * `r d[1]/1000`^2/4 * `r v[1]` + `r q[3]/1000`)/(pi * `r d[2]/1000`^2/4) = `r v[2]`


Meta-information
================
extype: num
exsolution: `r v[2]`
exname: vazao
extol: 0.2